<?php
/**
 * Created by PhpStorm.
 * User: Ezytli
 * Date: 31.05.2017
 * Time: 17:15
 */

namespace exbico\gateway\smsonline;

class NewFeature extends SMSOnlineConnector
{
    public $feature;

    public function __construct(SMSOnlinePostRequest $feature)
    {
        $this->feature = $feature;
    }
}